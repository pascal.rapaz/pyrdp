# PyRDP (Deprecated)

> Since the latest version of Remmina is available, this project is deprecated.
> 
> You can download and install the version 1.2.x of Remmina following instructions at [Remmina Wiki](https://github.com/FreeRDP/Remmina/wiki)

This tool use xfreerdp to establish a RDP connection through a gateway.

This is a temporary tool and it will be removed when Remmina 1.2.x will be officialy available

## Usage

1. Check out the PyRDP sources

   `$ git clone https://github.com/RapazP/pyrdp.git`

2. Create an empty configuration file : `config.json`

   `$ python pyrdp.py -C`

3. Encrypt the password used for the connection

   `$ python pyrdp.py -e <password>`

   The password encryption just prevents to be in clear in the configuration file. I strongly advise to restict access to the `config.json` file to increase the security (i.e. `chmod 0600 config.json`)

4. Copy/paste the encrypted password into the configuration file (`config.json`) and complete connection informations.

   Multiples drives can be added in `drive` list. You can also leave empty `drive` if you don't want any specific share in Windows.

5. Connect to the RDP with the command `python pyrdp.py`

## PyRDP command options

```
usage: pyrdp.py [-h] [-C] [-e ENCODE] [-m] [-s SIZE] [-f] [-v] [-V]

Use xfreerdp to establish a RDP connection through a gateway

optional arguments:
 -h, --help            show this help message and exit
 -C, --createconfig    create an empty configuration file
 -e ENCODE, --encode ENCODE
                       return the encrypted password
 -m, --multimon        enable RDP multi-screen support
 -s SIZE, --size SIZE  define the RDP screen size <width>x<height> (default
                       in configuration file is 1280x1024)
 -f, --fullscreen      start RDP in full screen (Ctrl+Alt+Enter to exit full
                       screen)
 -v, --verbose
 -V, --version         show program's version number and exit
```
