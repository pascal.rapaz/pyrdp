#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##
# -----------------------------------------------------------------------------
# (c) 2015 by Pascal Rapaz
# -----------------------------------------------------------------------------
#
# Open a xfreerdp session
#
# Best resolution for my ubuntu 16.04 (launcher size 24):
#   - 1920x1080 --> 1878x1026
#
# Manual :
#   https://github.com/awakecoding/FreeRDP-Manuals/blob/master/User/FreeRDP-User-Manual.markdown
#
# License: GPL-2+
#   This package is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This package is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   If you need the GNU General Public License write to:
#     Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
#     MA 02110-1301, USA.
#
# (c) 2015, Pascal Rapaz (RapazP - pascal.rapaz@rapazp.ch)
# =============================================================================
##

import argparse
import textwrap
import os
import json
import sys
import base64

from subprocess import call

VERSION = "1.1.0"
VERSION_MSG = "Application     : %s" % __file__ +\
              "\nRelease version : %s" % VERSION +\
              "\n(C) 2015 - 2016 by Pascal Rapaz (pascal.rapaz@gmail.com)"

WORKING_DIR = os.path.dirname(os.path.realpath(__file__))

CONFIG = 'config.json'
FULL_CONFIG_PATH = os.path.join(WORKING_DIR, CONFIG)


def connect():
  data = json.load(open(FULL_CONFIG_PATH))

  # prepare some options
  drives = ",".join(data['drive'])
  size = args.size if (args.size) else data['size']

  if (args.halfsize):
    s = size.split("x")
    size = str(int(s[0])/2) + "x" + s[1]
  #endIf

  # replace string and execute command
  cmd = ["xfreerdp",
        "/v:%s" % data['server'],
        "/u:%s@%s" % (data['user'], data['domain']),
        "/p:%s" % base64.b64decode(data['password']).decode(),
        "/g:%s" % data['gateway'],
        "/size:%s" % size,
        "+clipboard",
        "+compression",
        "/printer",
        "/bpp:32",
        "+fonts",
        "/kbd:0x100C"
        ]

  for k, v in data['drive'].items():
    cmd.append("/drive:%s,%s" % (k, v))
  #endFor

  if (args.fullscreen):
    cmd.append("/f")
  #endIf

  if (args.multimon):
    cmd.append("/multimon")
  #endIf

  if (args.verbose):
    print("[COMMAND] " + ' '.join(cmd))
  #endIf

  sys.exit(
    call(cmd)
  )
#endDef

def _create_configuration_file():

  if (os.path.isfile(FULL_CONFIG_PATH)):
    print("[ERROR] Configuration file already exists in %s" % FULL_CONFIG_PATH)
    sys.exit(1)
  #endIf

  with open(FULL_CONFIG_PATH, "w") as outfile:
    json.dump({
      "server": "",
      "domain": "",
      "gateway": "",
      "user": "",
      "password": "",
      "size":"1280x1024",
      "drive": {
        "tmp": "/tmp"
      }
    }, outfile, indent=2)
  #endWith
#endDef

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Startup methods
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_arguments():
  """
  Retrieve command line arguments
  """

  parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                   description=textwrap.dedent("""
    Use xfreerdp to establish a RDP connection through a gateway
    """))

  parser.add_argument("-C", "--createconfig",
                      action="store_true",
                      help="create an empty configuration file and exit. For security reason, I strongly advise to restict access to the `config.json` (i.e. chmod 0600 config.json)")

  parser.add_argument("-e", "--encode",
                      action="store",
                      help="return the encrypted password and exit")

  parser.add_argument("-m", "--multimon",
                      action="store_true",
                      help="enable RDP multi-screen support")

  parser.add_argument("-s", "--size",
                      action="store",
                      help="define the RDP screen size <width>x<height> (default in configuration file is 1280x1024)")

  parser.add_argument("-f", "--fullscreen",
                      action="store_true",
                      help="start RDP in full screen (Ctrl+Alt+Enter to exit full screen)")

  parser.add_argument("-H", "--halfsize",
                        action="store_true",
                        help="divide horizontal size defined in config by 2")

  parser.add_argument("-v", "--verbose",
                      action='store_true',
                      help="increase execution verbosity")

  parser.add_argument("-V", "--version",
                      action='version',
                      version=VERSION_MSG)

  return parser.parse_args()
#endDef

if __name__ == "__main__":
  args = get_arguments()

  if (args.encode):
    print("Encrypted password : %s" % base64.b64encode(args.encode.encode()).decode())
    sys.exit(0)
  #endIf

  if (args.createconfig):
    _create_configuration_file()
    sys.exit(0)
  #endIf

  connect()
#endIf
